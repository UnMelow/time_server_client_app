import socket


def main():
    connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP)
    connection.connect((input("Введите ip адрес сервера: "), 1303))
    received_data = connection.recv(1024)
    print(received_data.decode("utf-8"))
    connection.close()


if __name__ == '__main__':
    main()
